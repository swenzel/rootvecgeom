// Sandro Wenzel (CERN); November 2016

#ifndef TGEOVECGEOMNAVIGATOR
#define TGEOVECGEOMNAVIGATOR

// A class navigator class mimicking the TGeoNavigator interface
// but dispatching internally to VecGeom
// only few functions will be implemented with VecGeom
// at the beginning

// A difficulty comes from the fact that TGeoNavigator does not
// define virtual interfaces; clients must hence choose the concrete
// Navigator type they want to talk to

// in order not to add overhead; trying to stick to an inline
// implementation model

#include "TGeoNavigator.h"
#include "navigation/NavigationState.h"
#include "base/Vector3D.h"
#include <iostream>

class TGeoVecGeomNavigator : public TGeoNavigator {
public:
  static bool Setup();

  // default constructor
  // needs closed gGeoManager object ?
  TGeoVecGeomNavigator()
      : fStep(0), fSafety(0.), fLastPoint(), fPoint(), fDirection(), fPath(), fIsSameLocation(false),
        fCurrentNode(nullptr), fNextNode(nullptr)
  {
    Init();
  }
  TGeoVecGeomNavigator(TGeoManager *mgr)
      : fStep(0), fSafety(0.), fLastPoint(), fPoint(), fDirection(), fPath(), fIsSameLocation(false),
        fCurrentNode(nullptr), fNextNode(nullptr), TGeoNavigator(mgr)
  {
    Init();
  }
  using TGeoNavigator::TGeoNavigator;

  // the main navigation methods ( we need to make sure that it matches the TGeo interface ):
  TGeoNode *FindNextBoundaryAndStep(double /*stepmax*/, bool comps = false);

  // A simple FindNextBoundary interface
  TGeoNode *FindNextBoundary(double stepmax = vecgeom::kInfLength);

  TGeoNode const *InitTrack(const Double_t *point, const Double_t *dir)
  {
    return TGeoVecGeomNavigator::InitTrack(point[0], point[1], point[2], dir[0], dir[1], dir[2]);
  }

  TGeoNode const *InitTrack(Double_t x, Double_t y, Double_t z, Double_t nx, Double_t ny, Double_t nz)
  {
    SetCurrentPoint(x, y, z);
    SetCurrentDirection(nx, ny, nz);
    return TGeoVecGeomNavigator::FindNode();
  }

  TGeoNode const *GetCurrentNode() const;
  TGeoNode const *GetNextNode() const;
  TGeoVolume const *GetCurrentVolume() const;

  TGeoNode const *FindNode(Bool_t safe_start = kTRUE);
  TGeoNode const *FindNode(Double_t x, Double_t y, Double_t z);

  double GetStep() const { return fStep; }
  int GetLevel() const;
  bool IsOnBoundary() const { return fCurrstate->IsOnBoundary(); }
  bool IsOutside() const { return fCurrstate->IsOutside(); }

  void SetCurrentPoint(double x, double y, double z) { fPoint.Set(x, y, z); }
  void SetCurrentDirection(double x, double y, double z) { fDirection.Set(x, y, z); }
  const double *GetCurrentPoint() const { return &fPoint.x(); }
  const double *GetCurrentDirection() const { return &fDirection.x(); }
  const double *GetLastPoint() const { return &fLastPoint.x(); }
  double GetLastSafety() const { return fLastSafety; }

  double GetSafeDistance() const { return fSafety; }

  // interfaces from TGeoNavigator to do coordinate transformations
  void LocalToMaster(const Double_t *local, Double_t *master) const;
  void LocalToMasterVect(const Double_t *local, Double_t *master) const;
  void LocalToMasterBomb(const Double_t *local, Double_t *master) const;
  void MasterToLocal(const Double_t *master, Double_t *local) const;
  void MasterToLocalVect(const Double_t *master, Double_t *local) const;
  void MasterToLocalBomb(const Double_t *master, Double_t *local) const;

  // fill array "names" with "daughter - indices" at each level of NavigationState?
  void GetBranchNames(Int_t *names) const;
  void GetBranchNumbers(Int_t *copyNumbers, Int_t *volumeNumbers) const;

  // return mother node "up" levels up
  TGeoNode const *GetMother(size_t up = 1) const;

  // IsSameLocation (checks if current INTERNAL point is still compatible with current node)
  bool IsSameLocation() const { return fIsSameLocation; }

  // IsSameLocation (checks of global point x, y, z is still compatible with current node
  // we may update the internally stored point to the new position
  bool IsSameLocation(double x, double y, double z, bool updateCurrentPoint = false);

  // return volume path (of current navigation state) as string
  const char *GetPath() const;

  double *FindNormalFast();
  double *FindNormal(bool dummy=false) { return FindNormalFast(); }

  // GetCurrentMatrix ??

  // manipulation of the navigation states
  void CdTop();
  void CdDown(size_t index);

  void Clear();

  // computes safe distance for the current point
  double Safety(bool inside = false);

private:
  void Init();

  double fStep;
  double fLastSafety = 0.; // last known safety (w.r.t. to fLastPoint)
  double fSafety;

  // the state is kept in navigation states
  vecgeom::NavigationState *fState     = nullptr;
  vecgeom::NavigationState *fNewstate  = nullptr;
  vecgeom::NavigationState *fCurrstate = nullptr;

  vecgeom::Vector3D<double> fPoint;     // current position
  vecgeom::Vector3D<double> fLastPoint; // last known position
  vecgeom::Vector3D<double> fDirection; // current direction

  // think about caching the current global transformations here
  // needs interface in navigator that does not transform point

  mutable std::string fPath;

  bool fIsSameLocation; // flag checking if a new location (FindNode) is the same as the previous location (if any)
  TGeoNode *fCurrentNode;
  TGeoNode *fNextNode;
  vecgeom::Vector3D<double> fNormal;

}; // end class

// inline implementation
inline TGeoNode const *TGeoVecGeomNavigator::FindNode(Double_t x, Double_t y, Double_t z)
{
  SetCurrentPoint(x, y, z);
  return TGeoVecGeomNavigator::FindNode();
}

inline TGeoNode const *TGeoVecGeomNavigator::GetCurrentNode() const
{
  return fCurrentNode;
}

inline TGeoNode const *TGeoVecGeomNavigator::GetNextNode() const
{
  return fNextNode;
}

inline int TGeoVecGeomNavigator::GetLevel() const
{
  return fCurrstate->GetCurrentLevel();
}

inline TGeoVolume const *TGeoVecGeomNavigator::GetCurrentVolume() const
{
  return TGeoVecGeomNavigator::GetCurrentNode()->GetVolume();
}

inline void TGeoVecGeomNavigator::LocalToMaster(const Double_t *local, Double_t *master) const
{
  using vecgeom::Transformation3D;
  using vecgeom::Vector3D;
  Transformation3D m;
  fCurrstate->TopMatrix(m);
  auto r    = m.InverseTransform(Vector3D<double>(local[0], local[1], local[2]));
  master[0] = r[0];
  master[1] = r[1];
  master[2] = r[2];
}

inline void TGeoVecGeomNavigator::LocalToMasterVect(const Double_t *local, Double_t *master) const
{
  using vecgeom::Transformation3D;
  using vecgeom::Vector3D;
  Transformation3D m;
  fCurrstate->TopMatrix(m);
  auto r    = m.InverseTransformDirection(Vector3D<double>(local[0], local[1], local[2]));
  master[0] = r[0];
  master[1] = r[1];
  master[2] = r[2];
}

inline void TGeoVecGeomNavigator::MasterToLocal(const Double_t *master, Double_t *local) const
{
  using vecgeom::Transformation3D;
  using vecgeom::Vector3D;
  Transformation3D m;
  fCurrstate->TopMatrix(m);
  auto r   = m.Transform(Vector3D<double>(master[0], master[1], master[2]));
  local[0] = r[0];
  local[1] = r[1];
  local[2] = r[2];
}

inline void TGeoVecGeomNavigator::MasterToLocalVect(const Double_t *master, Double_t *local) const
{
  using vecgeom::Transformation3D;
  using vecgeom::Vector3D;
  Transformation3D m;
  fCurrstate->TopMatrix(m);
  auto r   = m.TransformDirection(Vector3D<double>(master[0], master[1], master[2]));
  local[0] = r[0];
  local[1] = r[1];
  local[2] = r[2];
}

inline TGeoNode const *TGeoVecGeomNavigator::GetMother(size_t up) const
{
  const auto l = fCurrstate->GetCurrentLevel() - 1;
  assert(fCurrstate->At(l) == fCurrstate->Top());
  if (int(l - up) >= 0) {
    return vecgeom::RootGeoManager::Instance().tgeonode(fCurrstate->At(l - up));
  }
  return nullptr;
}

inline void TGeoVecGeomNavigator::CdTop()
{
  // modify current navigation state to represent top volume
  while (fCurrstate->GetCurrentLevel() > 1) {
    fCurrstate->Pop();
  }
}

inline void TGeoVecGeomNavigator::CdDown(size_t daughterindex)
{
  // put some protection here
  if (!fCurrstate->IsOutside()) {
    auto &daughters = fCurrstate->Top()->GetLogicalVolume()->GetDaughters();
    auto size       = daughters.size();
    if (daughterindex < size) {
      fCurrstate->Push(daughters[daughterindex]);
    }
  }
}

inline double *TGeoVecGeomNavigator::FindNormalFast()
{
  using vecgeom::Vector3D;
  if(!fNextNode) return nullptr;
  Vector3D<double> lpoint;
  Vector3D<double> ldir;
  double lnorm[3];

  // TODO: optimal cached treatment of this matrix
  vecgeom::Transformation3D m;
  fNewstate->TopMatrix(m);

  m.Print();
  m.Transform(fPoint, lpoint);
  m.TransformDirection(fDirection, ldir);

  // dispatch to ROOT for the moment;
  // TODO: dispatch to VecGeom
  fNextNode->GetVolume()->GetShape()->ComputeNormal(&lpoint.x(), &ldir.x(), lnorm);

  m.InverseTransformDirection(Vector3D<double>(lnorm[0],lnorm[1],lnorm[2]), fNormal);
  return &fNormal.x();
}

// The following list are interfaces of TGeoManager/TGeoNavigator which are accessed from
// Geant3:
// TGeant3.cxx:we have to go to gGeoManager->GetVolume("name") which scans a list of
// gGeoManager->FindNormal(kFALSE); --> GetCurrentNavigator
// gGeoManager->FindNormalFast(); --> GetCurrentNavigator
// gGeoManager->GetLevel(); --> GetCurrentNavigator (DONE)
// gGeoManager->GetMother(level-fNextVol); --> GetCurrentNavigator (DONE)
// gGeoManager->IsOutside(); --> GetCurrentNavigator (DONE)
// gGeoManager->GetCurrentNode(); --> GetCurrentNavigator (DONE)
// gGeoManager->GetTopVolume(); (already ok)
// gGeoManager->GetCurrentVolume(); --> GetCurrentNavigator (DONE)
// gGeoManager->SetCurrentPoint(pt); --> GetCurrentNavigator (DONE)
// gGeoManager->SetCurrentDirection(x[3],x[4],x[5]); --> GetCurrentNavigator (DONE)

// gGeoManager->GetPath(); --> GetCurrentNavigator (DONE)
// gGeoManager->LocalToMaster(XD,XM); (DONE)
// gGeoManager->LocalToMasterVect(XD,XM); (DONE)
// gGeoManager->FindNode(x[0],x[1],x[2]); (DONE)

// TGeant3TGeo.cxx:      gGeoManager->GetBranchNames(gcvolu->names);
// TGeant3TGeo.cxx:      gGeoManager->GetBranchNumbers(gcvolu->number,gcvolu->lvolum);
// gGeoManager->MasterToLocal(XM,XD); --> GetCurrentNavigator() (DONE)
// gGeoManager->MasterToLocalVect(XM,XD); --> GetCurrentNavigator() (DONE)
// gGeoManager->IsSameLocation(x[0], x[1], x[2]) --> GetCurrentNavigator (DONE)
// gGeoManager->IsSameLocation() --> GetCurrentNavigator (DONE)
// gGeoManager->FindNode(); --> GetCurrentNavigator (DONE)
// gGeoManager->IsCurrentOverlapping()
// gGeoManager->CdTop(); (DONE)
// gGeoManager->CdDown(id); (DONE
// gGeoManager->FindNextBoundary(-step); --> GetCurrentNavigator (NOT YET DONE)
// gGeoManager->FindNextBoundary(step); --> GetCurrentNavigator (NOT YET DONE)
// gGeoManager->GetSafeDistance(); --> (DONE)
// gGeoManager->GetStep(); --> GetCurrentNavigator (DONE)

#endif
