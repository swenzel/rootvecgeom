#include "TGeoNode.h"
#include "TGeoManager.h"
#include "management/RootGeoManager.h" // from VecGeom -> load vecgeom geometry
#include "management/GeoManager.h"

#include <cmath>
#include "TGeoMaterial.h"
#include "TGeoNavigator.h"
#include "TMath.h"

#include "TGeoVecGeomNavigator.h"

// for point generation
#include "base/SOA3D.h"
#include "volumes/utilities/VolumeUtilities.h"

#include "base/Stopwatch.h"

//
//#ifdef 1
//using Navigator_t = TGeoNavigator;
//#else
using Navigator_t = TGeoVecGeomNavigator;
//#endif

// a globaly known navigator instance
Navigator_t *gNavigator;

// This program demonstrates that we can use VecGeom behind
// a ROOT Navigator interface (with minimal effort)
// The demonstrating example is taken from a piece of AliRoot@ALICE
// which calculates some material budget in reconstruction

Navigator_t * GetCurrentNavigator() {
    return (Navigator_t *)gGeoManager->GetCurrentNavigator();
}

// Problem to solve:
// many poeple use TGeoManager as highest level interface, which holds some navigators
// --> eventually TGeoManager needs to be configurable to use either TGeoNavigator or TGeoVecGeomNavigator

Double_t MeanMaterialBudget(const Double_t * __restrict__ start, const Double_t * __restrict__ end, Double_t * __restrict__ mparam)
{
  //
  // Calculate mean material budget and material properties between
  //    the points "start" and "end" in the geometry.
  //
  // Code taken from ALIROOT

  mparam[0]=0; mparam[1]=1; mparam[2]=0; mparam[3]=0;
  mparam[4]=0; mparam[5]=0; mparam[6]=0;

  auto tol = TGeoShape::Tolerance();

  //
  Double_t bparam[6] = {}; // total parameters -- zero initialized
  Double_t lparam[6]; // local parameters

  //
  Double_t length;
  Double_t dir[3];
  length = TMath::Sqrt((end[0]-start[0])*(end[0]-start[0])+
                       (end[1]-start[1])*(end[1]-start[1])+
                       (end[2]-start[2])*(end[2]-start[2]));
  mparam[4]=length;
  if (length < tol) return 0.0;

  Double_t invlen = 1./length;
  dir[0] = (end[0]-start[0])*invlen;
  dir[1] = (end[1]-start[1])*invlen;
  dir[2] = (end[2]-start[2])*invlen;

  auto fixlparam = [&] (TGeoNode const *node) {
    TGeoMaterial const *material = node->GetVolume()->GetMedium()->GetMaterial();
    lparam[0]   = material->TGeoMaterial::GetDensity();
    lparam[1]   = material->TGeoMaterial::GetRadLen();
    lparam[2]   = material->TGeoMaterial::GetA();
    lparam[3]   = material->TGeoMaterial::GetZ();
    lparam[4]   = length;
    lparam[5]   = lparam[3]/lparam[2];
    if (material->IsMixture()) {
      TGeoMixture * mixture = (TGeoMixture*)material;
      lparam[5] = 0;
      Double_t sum = 0;
      for (Int_t iel = 0;iel<mixture->TGeoMixture::GetNelements();iel++){
        sum  += mixture->GetWmixt()[iel];
        lparam[5]+= mixture->GetZmixt()[iel]*mixture->GetWmixt()[iel]/mixture->GetAmixt()[iel];
      }
      lparam[5]/=sum;
    }
  };

  // Initialize start point and direction
  TGeoNode const *currentnode = 0;
  auto nav = GetCurrentNavigator();
  TGeoNode const *startnode = nav->InitTrack(start, dir);
  if (!startnode) {
    return 0.0;
  }
  fixlparam(startnode);

  // Locate next boundary within length without computing safety.
  // Propagate either with length (if no boundary found) or just cross boundary
  nav->FindNextBoundaryAndStep(length, kFALSE);
  Double_t step = 0.0; // Step made
  Double_t snext = nav->GetStep();
  // If no boundary within proposed length, return current density
  if (!nav->IsOnBoundary()) {
    mparam[0] = lparam[0];
    mparam[1] = lparam[4]/lparam[1];
    mparam[2] = lparam[2];
    mparam[3] = lparam[3];
    mparam[4] = lparam[4];
    return lparam[0];
  }
  // Try to cross the boundary and see what is next
  Int_t nzero = 0;
  while (length>tol) {
    currentnode = nav->GetCurrentNode();
    if (snext<2.*tol) nzero++;
    else nzero = 0;
    if (nzero>3) {
      // This means navigation has problems on one boundary
      // Try to cross by making a small step
      //static int show_error = !(getenv("HLT_ONLINE_MODE") && strcmp(getenv("HLT_ONLINE_MODE"), "on") == 0);
      //if (show_error) AliErrorClass("Cannot cross boundary\n");
      auto invstep = step; // avoid too many divisions
      mparam[0] = bparam[0]*invstep;
      mparam[1] = bparam[1];
      mparam[2] = bparam[2]*invstep;
      mparam[3] = bparam[3]*invstep;
      mparam[5] = bparam[5]*invstep;
      mparam[4] = step;
      mparam[0] = 0.;             // if crash of navigation take mean density 0
      mparam[1] = 1000000;        // and infinite rad length
      return bparam[0]*invstep;
    }
    mparam[6]+=1.;
    step += snext;
    bparam[1] += snext/lparam[1];
    bparam[2] += snext*lparam[2];
    bparam[3] += snext*lparam[3];
    bparam[5] += snext*lparam[5];
    bparam[0] += snext*lparam[0];

    if (snext>=length) break;
    if (!currentnode) break;
    length -= snext;
    fixlparam(currentnode);

    // next step;
    nav->FindNextBoundaryAndStep(length, kFALSE);
    snext = nav->GetStep();
  }
  auto invstep = 1./step;
  mparam[0] = bparam[0]*invstep;
  mparam[1] = bparam[1];
  mparam[2] = bparam[2]*invstep;
  mparam[3] = bparam[3]*invstep;
  mparam[5] = bparam[5]*invstep;
  return bparam[0]*invstep;
}


int main() {
  // load ALICE geometry
  TGeoManager::Import("alice.root");
  vecgeom::RootGeoManager::Instance().LoadRootGeometry();
  // create TGeoVecGeomNavigator

  // set a navigator


  auto navlist = gGeoManager->GetListOfNavigators();
  navlist->AddAt(new Navigator_t(gGeoManager),0);
  gGeoManager->SetCurrentNavigator(0);
  auto nav = gGeoManager->GetCurrentNavigator();
  nav->BuildCache(true,false);
  nav->GetCache()->BuildInfoBranch();

  // generate arbitrary point pairs inside the detector (uses VecGeom)
  int np(1000);
  vecgeom::SOA3D<double> localpoints(np), globalpoints(np);
  auto lvol = vecgeom::GeoManager::Instance().FindLogicalVolume("ALIC");
  if (lvol){
    lvol->Print();
    vecgeom::volumeUtilities::FillGlobalPointsForLogicalVolume<vecgeom::SOA3D<double>>(lvol,localpoints,globalpoints,
                                                                                       np);
  }

  vecgeom::Stopwatch timer;
  timer.Start();
  double mparam[5];
  double accum = 0.;
  for (int i=0; i<np; ++i) {
    for (int j=0; j<np; ++j) {
       auto x=globalpoints[i];
       auto y=globalpoints[j];
       accum += MeanMaterialBudget(&x[0], &y[0], mparam);
    }
  }
  timer.Stop();
  std::cerr << "accumulated material budget " << accum << "\t" << timer.Elapsed() << "\n";
  return 0;
}
