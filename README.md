# README #

This is a prototype trying to hide VecGeom behind a TGeo-like navigator interface.
The main goals are:

- provide a quick solution to be able to dispatch to VecGeom navigation
  from software already setup with TGeo and which talks to TGeo navigation

- research ways how this can be done with minimal effort ... with possible later integration into ROOT itself.

### How do I get set up? ###

- install ROOT (github.com/root/root.git)
- install VecGeom (gitlab.cern.ch/VecGeom/VecGeom.git)
- cmake -DVecGeom_DIR=... -DROOT_Dir=... (in some build directory)